#! /usr/bin/python
import sys

print (" _____  _                  _____             _     _____         _               _         _                ")
print ("/  ___|| |                |_   _|           | |   /  __ \       | |             | |       | |               ")
print ("\ `--. | |_   ___  _ __     | |    ___  ___ | |_  | /  \/  __ _ | |  ___  _   _ | |  __ _ | |_   ___   _ __ ")
print (" `--. \| __| / _ \| '_ \    | |   / _ \/ __|| __| | |     / _` || | / __|| | | || | / _` || __| / _ \ | '__|")
print ("/\__/ /| |_ |  __/| |_) |   | |  |  __/\__ \| |_  | \__/\| (_| || || (__ | |_| || || (_| || |_ | (_) || |   ")
print ("\____/  \__| \___|| .__/    \_/   \___||___/ \__|  \____/ \__,_||_| \___| \__,_||_| \__,_| \__| \___/ |_|   ")
print ("                  | |                                                                                       ")
print ("                  |_|                                                                Version 0.1 by BJ Marr ")
print ("\nFor more information regarding a cycling Step Test, please visit my blog post at https://bjmarr.uk/step-test-calculator/ or")
print ("go to GPLama's website at https://gplama.blogspot.co.uk/2016/07/how-to-do-step-test-on-zwift-pb-hptek.html\n")
print ("The Zwift workout file can be found here: http://www.zwiftcoach.com/ZWIFT_STEP_TEST_pb_HPTek.zwo\n")
print ("The workout must consist of 25w increments every 2 minutes and 30 seconds.")
print ("You must mark down your last completed stage, your uncompleted stage and how long into that stage you got ( in seconds)\n\n")

def get_float(prompt):
    while True:
        try:
            return float(raw_input(prompt))
        except ValueError, e:
            print "Invalid input"

def step_test_calc(completed, duration):
    stage_percentage = duration / 150
    partial_stage = stage_percentage * 25
    step_final = completed + partial_stage
    estimated_ftp = step_final * 0.825
    ftp_test_target = estimated_ftp * 1.05

    print ("\n\nYour estimated FTP is: %1.1f" %estimated_ftp)
    print ("\nYour target for your next FTP test should be: %1.1f" %ftp_test_target)

# raw_input returns the empty string for "enter"
yes = {'yes', 'y', 'ye', ''}
no = {'no', 'n'}

choice = raw_input("Do you wish to calculate your FTP now? [y/n]: ").lower()

if choice in yes:

    completed_stage = get_float("Please enter the stage that you last completed in watts (e.g. 300): ")
    duration_of_uncomplete = get_float("Please enter how long you managed to ride into that stage in seconds (e.g. 27): ")
    step_test_calc(completed_stage, duration_of_uncomplete)
elif choice in no:
    exit
else:
    sys.stdout.write("Please respond with 'yes' or 'no'")



